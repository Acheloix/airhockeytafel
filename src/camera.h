#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace std;
using namespace cv;

class Camera{
  private:
    VideoCapture stream;
    bool debug;
  public:
    Camera(int);
    Mat takePicture();
    Mat getImg();
};
