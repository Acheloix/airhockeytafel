#include <iostream>
#include <cmath>
#include "math.h"
using namespace std;

struct resultWrap{
  float x;
  float y;
  float angle;
};


Math::Math(int _maxSteps){
  stepsize=(yrange[1]-yrange[0])/_maxSteps; //stepsize = distance/steps
  cout<<"Math initialized"<<endl;
}

void Math::initialize(int _coordinates[2][2]){ //constructor w values
  midPointY=(yrange[1]+yrange[0])/2;
  for (int i=0;i<2;i++){
    xArr[i]=xrange[1]-(_coordinates[i][0]-xrange[0]);
    yArr[i]=_coordinates[i][1];
  }
}

int Math::calculate(){
  float goal;
  calcDelta();
  deg=calcAngle();
  if(deg==10000){
   return (int)(midPointY/stepsize);
  }
  float res=calcCollision();
  if(res==10000){
   return (int)(midPointY/stepsize);
  }
  return calcSteps();
}

void Math::calcDelta(){
  dx=xArr[1]-xArr[0];
  dy=yArr[1]-yArr[0];
}

float Math::calcAngle(){
  float result;
  if(dx==0 && dy==0){
    return 10000;
  }
  else if(dx==0){
    result=0;
  }
  else if(dx<0){
    result=atan(dy/dx)*(180/M_PI)+180;
  }
  else if(dx>0){
    result=atan(dy/dx)*(180/M_PI);
  }
  else{
    return 10000;
  }

  if(result<0){
    result=result+360;
  }
  return result;
}

int Math::calcSteps(){
  int keeperGoal;
  if ((xArr[2] == xrange[0]) && (goalRange[0] < yArr[2]) && (yArr[2] < goalRange[1])){
    keeperGoal = yArr[2];
  }
  else if ((xArr[3] == xrange[0]) && (goalRange[0] < yArr[3]) && (yArr[3] < goalRange[1])){
    keeperGoal = yArr[3];
  }
  else if ((xArr[4] == xrange[0]) && (goalRange[0] < yArr[4]) && (yArr[4] < goalRange[1])){
    keeperGoal = yArr[4];
  }
  else{
    keeperGoal=midPointY;
  }
  return (int)(keeperGoal/stepsize);
}

float Math::calcCollision(){
  struct resultWrap result;
  if(deg<0){
    deg=deg+360;
  }
  if(deg<=90){
    result=calcFirstQuad(deg,xArr[1],yArr[1]);
    xArr[2]=result.x;
    yArr[2]=result.y;
    if(result.angle>=90&&result.angle<180){
      result=calcSecondQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle<=90){
        result=calcFirstQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=180&&result.angle<=270){
        result=calcThirdQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
    else if(result.angle>=270&&result.angle<360){
      result=calcFourthQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle<=90){
        result=calcFirstQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=180&&result.angle<=270){
        result=calcThirdQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
  }
  else if(deg>90&&deg<=180){
    result=calcSecondQuad(deg,xArr[1],yArr[1]);
    xArr[2]=result.x;
    yArr[2]=result.y;
    if(result.angle<=90&&result.angle>=0){
      result=calcFirstQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle>=90&&result.angle<=180){
        result=calcSecondQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=270&&result.angle<360){
        result=calcFourthQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
    else if(result.angle>=180&&result.angle<=270){
      result=calcThirdQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle>=90&&result.angle<=180){
        result=calcSecondQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=270&&result.angle<360){
        result=calcFourthQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
  }
  else if(deg>180&&deg<=270){
    result=calcThirdQuad(deg,xArr[1],yArr[1]);
    xArr[2]=result.x;
    yArr[2]=result.y;
    if(result.angle>=90&&result.angle<=180){
      result=calcSecondQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle<=90&&result.angle>=0){
        result=calcFirstQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=180&&result.angle<=270){
        result=calcThirdQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
    else if(result.angle>=170&&result.angle<360){
      result=calcFourthQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle<=90&&result.angle>=0){
        result=calcFirstQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=180&&result.angle<=270){
        result=calcThirdQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
  }
  else if(deg>270){
    result=calcFourthQuad(deg,xArr[1],yArr[1]);
    xArr[2]=result.x;
    yArr[2]=result.y;
    if(result.angle<=90&&result.angle>=0){
      result=calcFirstQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle>=90&&result.angle<=180){
        result=calcSecondQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=270&&result.angle<360){
        result=calcFourthQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
    else if(result.angle>=180&&result.angle<=270){
      result=calcThirdQuad(result.angle,xArr[2],yArr[2]);
      xArr[3]=result.x;
      yArr[3]=result.y;
      if(result.angle>=90&&result.angle<=180){
        result=calcSecondQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
      else if(result.angle>=270&&result.angle<360){
        result=calcFourthQuad(result.angle,xArr[3],yArr[3]);
        xArr[4]=result.x;
        yArr[4]=result.y;
      }
    }
  }
  if(!result.angle){
    return 10000;
  }
  else{
    return result.angle;
  }
}

struct resultWrap Math::calcFirstQuad(float angle, float x, float y){
  float xcol=x+((yrange[1]-y)/(tan(angle*(M_PI/180))));
  float ycol=y+((xrange[1]-x)*(tan(angle*(M_PI/180))));
  float phi;
  float x2,y2;
  float resultAngle;
  struct resultWrap result;

  if(xrange[0]<=xcol&&xcol<=xrange[1]){
    phi=90-angle;
    resultAngle=270+phi;
    x2=xcol;
    y2=yrange[1];
  }
  else if(yrange[0]<=ycol&&ycol<=yrange[1]){
    phi=angle;
    resultAngle=180-phi;
    x2=xrange[1];
    y2=ycol;
  }
  else{
    phi,resultAngle,x2,y2=-1;
  }
  if(resultAngle<0&&resultAngle!=-1){
    resultAngle=resultAngle+360;
  }
  result.angle=resultAngle;
  result.x=x2;
  result.y=y2;
  return result;
}

struct resultWrap Math::calcSecondQuad(float angle,float x, float y){
 float xcol=x-((yrange[1]-y)/tan((180-angle)*M_PI/180));
 float ycol=y+x*tan((180-angle)*M_PI/180);
 float phi;
 float x2,y2;
 float resultAngle;
 struct resultWrap result;
 if(xrange[0]<=xcol&&xcol<=xrange[1]){
   phi=angle-90;
   resultAngle=270-phi;
   x2=xcol;
   y2=yrange[1];
 }
 else if(yrange[0]<=ycol&&ycol<=yrange[1]){
   phi=90-(angle-90);
   resultAngle=phi;
   x2=xrange[0];
   y2=ycol;
 }
 else{
   phi,resultAngle,x2,y2=-1;
 }
 if(resultAngle<0&&resultAngle!=-1){
   resultAngle=resultAngle+360;
 }
 result.angle=resultAngle;
 result.x=x2;
 result.y=y2;
 return result;
}

struct resultWrap Math::calcThirdQuad(float angle, float x, float y){
 float xcol=x-(y/tan((angle-180)*(M_PI/180)));
 float ycol=y-(x*tan((angle-180)*(M_PI/180)));
 float phi;
 float x2,y2;
 float resultAngle;
 struct resultWrap result;
 if(xrange[0]<=xcol&&xcol<=xrange[1]){
   phi=180-90-(angle-180);
   resultAngle=90+phi;
   x2=xcol;
   y2=yrange[0];
 }
 else if(yrange[0]<=ycol&&ycol<=yrange[1]){
   phi=180-90-(270-angle);
   resultAngle=360-phi;
   x2=xrange[0];
   y2=ycol;
 }
 else{
   phi,resultAngle,x2,y2=-1;
 }
 if(resultAngle<0&&resultAngle!=-1){
   resultAngle=resultAngle+360;
 }
 result.angle=resultAngle;
 result.x=x2;
 result.y=y2;
 return result;
}

struct resultWrap Math::calcFourthQuad(float angle, float x, float y){
 float xcol=x+(y/tan((360-angle)*M_PI/180));
 float ycol=y-(xrange[1]-x)*tan((360-angle)*M_PI/180);
 float phi;
 float x2,y2;
 float resultAngle;
 struct resultWrap result;
 if(xrange[0]<=xcol&&xcol<=xrange[1]){
   phi=180-(360-angle)-90;
   resultAngle=90-phi;
   x2=xcol;
   y2=yrange[0];
 }
 else if(yrange[0]<=ycol&&ycol<=yrange[1]){
   phi=360-angle;
   resultAngle=180+phi;
   x2=xrange[1];
   y2=ycol;
 }
 else{
   phi,resultAngle,x2,y2=-1;
 }
 if(resultAngle<0&&resultAngle!=-1){
   resultAngle=resultAngle+360;
 }
 result.angle=resultAngle;
 result.x=x2;
 result.y=y2;
 return result;
}
