#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace cv;

class CircleDetector{
  private:

  public:
    CircleDetector();
    Point detect(Mat);
};
