#include <iostream>
#include <future>
#include "circleDetector.h"
#include "camera.h"
#include "math.h"
#include "motor.h"
using namespace std;

int run(Math math, CircleDetector circleDetector, Camera camera,int oldPos){
  Mat img;
  int res,stepPos;
  Point coordinatesOld={-1,-1};

  for(;;){
    //take picture and see if there are circles
    Point coordinatesNew=circleDetector.detect(camera.takePicture());
    if(coordinatesOld.x!=-1){ //see if any previous coordinates have been saved
      int arr[2][2]={{coordinatesOld.x,coordinatesOld.y},
        {coordinatesNew.x,coordinatesNew.y}};
      math.initialize(arr);
      res=math.calculate();
      if(res!=10000){ //math.calculate() returns 10000 if error
        stepPos=res;
        break; //exit loop if valid result
      }
    }
    else{
      coordinatesOld=coordinatesNew;
    }
  }
  return stepPos;
}

int runMotor(int steps,int pos,int maxSteps, Motor motor, const atomic_bool& cancelled){
  int position=motor.move(steps,pos,maxSteps,ref(cancelled));
  return position;
}

int main(){
  atomic_bool cancelled; //tells the motor to stop if needed;
  Camera camera(0);
  Motor motor;
  int maxSteps=motor.calibrate(); //get stepsize
  Math math(maxSteps);
  int pos =(maxSteps+1)/2;
  int stepPos=(maxSteps+1)/2;
  int oldSteps=(maxSteps+1)/2;
  CircleDetector circleDetector;
 auto threadMotor = async(launch::async, runMotor,stepPos,pos,maxSteps,motor,ref(cancelled));

   for(;;){
      //create thread for motor
    stepPos = run(math,circleDetector,camera,pos);
    if(abs(stepPos-oldSteps)>=2){ //if movement needed
        cancelled=true; //stop motor
        oldSteps=stepPos;
        pos=threadMotor.get();
        cancelled=false; //reset cancelled
        threadMotor = async(launch::async, runMotor,stepPos,pos,maxSteps,motor,ref(cancelled));
   }
  }

  return 1;
}
