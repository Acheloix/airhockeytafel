class Math{
  private:
    int coordinates[2][2];
    float dx,dy,deg,stepsize;
    int midPointY;
    const float goalRange[2]{0,430};//{150,350};//approx range in front of the goal
    const float xrange[2]{0,777};//pixelrange table
    const float yrange[2]{0,430};
    float xArr[5],yArr[5];
  public:
    Math(int);
    void initialize(int[2][2]);
    int calculate();
    void calcDelta();
    float calcAngle();
    float calcCollision();
    struct resultWrap calcFirstQuad(float,float,float);
    struct resultWrap calcSecondQuad(float,float,float);
    struct resultWrap calcThirdQuad(float,float,float);
    struct resultWrap calcFourthQuad(float,float,float);
    int calcSteps();
};
