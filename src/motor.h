#include <atomic>
class Motor{
  private:
    int sleepPin = 3; //gpio3
    int stepPin = 2; //gpio2
    int directionPin = 0; //gpio0
    int buttonL = 4; //gpio23
    int buttonR =5; //gpio24
 public:
    Motor();
    int move(int,int,int,const std::atomic_bool&);
    int calibrate();
    void left();
    void right();
};
