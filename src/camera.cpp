#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include "camera.h"
using namespace std;
using namespace cv;


Camera::Camera(int i){
  stream.open("/dev/video"+to_string(i));
  if (!stream.isOpened()) { //check if video device has been initialised
    cout << "cannot open camera\n";
  }
  else{
    cout << "camera initialised\n";
  }
  debug=false;
}

Mat Camera::takePicture(){
  Mat color, img;
  stream.read(color);
  cvtColor(color, img, COLOR_BGR2GRAY ); //color to greyscale
  if(!img.empty()&&debug){
    imshow("image",img);
    waitKey(1);
  }
  return img;
}
