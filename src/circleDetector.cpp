#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "circleDetector.h"

using namespace cv;
using namespace std;

CircleDetector::CircleDetector(){
    cout<<"circledetector initialized\n";
}

Point CircleDetector::detect(Mat img){
  bool show=false; //set to true to show camera feed for debug purposes
  Point center(-1,-1);
  if(img.empty()){ //if no image return default error values
    return center;
  }
  vector<Vec3f> circles;
  // Apply the Hough Transform to find the circles
  HoughCircles(img, circles, HOUGH_GRADIENT, 1, 30, 200, 25, 0,0);
  if(circles.size()==1){ //if more than 1 circle is detected this is an error
      center=Point(cvRound(circles[0][0]), cvRound(circles[0][1])); //save coordinates

  }
  if(show){ //debug camera feed
    if(circles.size()==1){
      int radius = cvRound(circles[0][2]);
    //draw circles in camera feed
      circle( img, center, 3, Scalar(0,255,0), -1, 8, 0 );// circle center
      circle( img, center, radius, Scalar(0,0,255), 3, 8, 0 );// circle outline
    }
    namedWindow( "Hough Circle Transform", WINDOW_AUTOSIZE );
    imshow( "Hough Circle Transform", img );
    waitKey(1);
  }
  return center; //return coordinates
}
