#include <iostream>
#include "motor.h"
#include <wiringPi.h>
#include <atomic>
using namespace std;

Motor::Motor(){
  wiringPiSetup();
  pinMode(sleepPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(directionPin, OUTPUT);
  pinMode(buttonL, INPUT);
  pinMode(buttonR, INPUT);
  cout<<"Motor initialized"<<endl;

}
//
int Motor::calibrate(){
  cout<<"calibrating..."<<endl;
  int maxSteps=0;
  digitalWrite(sleepPin,HIGH);
  while(digitalRead(buttonR)==0){
    right();
  }
  delay(200);
  while(digitalRead(buttonL)==0){
    left();
    maxSteps++;
  }
  delay(200);
  for(int i=0;i<((maxSteps+1)/2);i++){
      	  right();
  }
  digitalWrite(sleepPin,LOW);
  return maxSteps;
}
//
int Motor::move(int stepPos,int pos,int maxSteps,const atomic_bool& cancelled){
   int position=pos;
   int steps=stepPos-pos;
    digitalWrite(sleepPin,HIGH);
    if(steps<0){
      for(int i=steps; i<0;i++){
         if(digitalRead(buttonR)==1){
		position=0;
		break;
	}
        if(cancelled){

          break;
        }
	        right();
	        position--;
      }
    }
    else if(steps>0){
      for(int i=steps; i>0;i--){
	if(digitalRead(buttonL)==1){
		position=maxSteps;
		break;
	}
        if(cancelled){

          break;
        }
	      left();
	      position++;
      }
   }
    digitalWrite(sleepPin,LOW);
    return position;

}

//
void Motor::left(){
  digitalWrite(directionPin,LOW);
  digitalWrite(stepPin,HIGH);
  delay(2);
  digitalWrite(stepPin,LOW);
  delay(5);
}
//
void Motor::right(){
  digitalWrite(directionPin,HIGH);
  digitalWrite(stepPin,HIGH);
  delay(2);
  digitalWrite(stepPin,LOW);
  delay(5);
}
